let canvas, ctx;

/**
 * Load the game.
 * @param {Scene} scene 
 */
function start(scene) {
  canvas = scene.load();
  ctx = canvas.getContext("2d");
  document.body.insertBefore(canvas, document.body.firstChild);
}

/**
 * Called when the website is loaded.
 */
function main() {
  start(mainScene);
  mainScene.setFps(100);
  mainScene.gameLoop(ctx);
}

main();